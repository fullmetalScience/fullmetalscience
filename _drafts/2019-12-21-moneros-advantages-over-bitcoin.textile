---
vim: spell spelllang=en
layout: post
class: post-template
cover: assets/images/spinning-top.jpg
title: "Monero's advantages over Bitcoin"
source: 
tags: [Articles]
author: fullmetalScience
---


Monero over personality-cults fluffypony => Tari => get out

TODO
No specific hardware has to be produced that has no use other than for mining
TODO
Cool how you modify the PyBitmessage config file with shell commands.
rbrunner7
https://www.reddit.com/r/Monero/comments/f81lbe/monero_multisig_runthrough_zerotomms_in_an_instant/fij7b8s/

M100: We sample the most recent 100 blocks in the blockchain and find the median block size. We set the variable M100 to the larger value in the set {median 100blocks, 300kB}

As of 2020-02-07 it's obvious at first glance that the median is below 300kB:
https://moneroblocks.info/stats/block-medians

Since no block penalty applies, the block reward remains at it's current XMR ~1.93:
Bactual: 1.93

base dynamic fee (?):
https://monero.stackexchange.com/questions/475/monero-inception-and-history-how-did-monero-get-started-what-are-its-origins-a/476#476


This is a fact-based composition of advantages Monero's advantages over Bitcoin
outline that protocol-level- and core software design decisions of 
analyzes the advantages brought by


h2. Usability

This is where Monero shines through good user experience.


h3. Using the network does not associate you with illegal activity

In Monero the transaction history is kept confidential, which makes every single Monero equal in value to every other Monero and you do not have to worry having your funds linked to an undesired spending history that you may not even be aware of.

The history of funds in Bitcoin can be traced back to the very moment they were mined. This might make you a suspect or even get you held accountable for the history of funds.


h3. All accounts can send to all other accounts

In Monero you hand out your account number (or your XMR.ID) and others will be able to send funds to you.

In Bitcoin, certain address types cannot receive funds from certain other types (see https://bitcoinelectrum.com/frequently-asked-questions/#what-is-the-difference-between-legacy-p2pkh-native-segwit-p2wpkh-or-p2wsh-and-p2sh-segwit-p2wpkh-p2sh).


h3. Per-payee account numbers are possible

In Monero one payee can send to the same account number repeatedly and you can then report on all payments received by payee (see the @incoming_transfers@ command).

In Bitcoin, a new address has to be used for each and every payment. You have to rely on secondary solutions to link the payments to your payments (see https://en.bitcoin.it/wiki/Address_reuse issues).


h3. Per-user accounts numbers are reasonable

In Monero you may instruct all your payees to send to the same account number. This allows you to use services like "XMR.ID":https://xmr.id to receive funds to _yourname@xmr.id_  without exposing your balances to your payees.

Bitcoin, by design, doesn't have the concept of a per-user fixed destination address. Since most people will never know that re-using addresses causes issues, the mistake will always be made.


h3. Transactions are cheaper

Taking valuation into account, as of 2019, a low-priority Monero transaction is roughly eight times cheaper than a low-priority Bitcoin transaction.

To be fair, looking at bare numbers and disregarding valuation, such a Bitcoin transaction is actually 17 times cheaper (BTC ~0.00000141 vs XMR ~0.00002450).


h3. Transactions get cheaper with increased use

In Monero transactions cost declines the more transactions happen on the network.

If transaction count increases in Bitcoin, the cost has at times become so high that using the network became unfeasible.


h3. Tiny amounts can be paid by contributing energy expenditure

In Monero, the mining algorithm allows for everyday hardware to pay micro-amounts in the form of work. The amounts payable a very limited in value, but they are free of transaction costs. (I like calling this feature ""_PoWeR_":https://www.reddit.com/r/Monero/comments/e9ix5r/cool_sexy_name_for_the_new_pay_rpc_thing/fap0ubz/".)

Bitcoin mining doesn't allow for consumer-type hardware to contribute.



h2. Security

+ Guessing is much harder in xmr since you have to scan the entire chain for incoming funds
~ dezentralisiert - client mined standartmäßig - ASIC resistant - CPU preferred (RandomX) - 
~ dezentralisiert - "On stage right now: people representing approximately 90% of the Bitcoin hashing power. Truly an historic moment.":https://twitter.com/lopp/status/673398201307664384
+ inflation
* Ed25519, the elliptic curve digital signature algorithm employed by Monero is considered potentially more secure than Bitcoin's secp256k1.


h2. Technically

+ atomic database lmdb
~ Flexibel (scheduled hard forks) -- exception
* 0.00000000 vs 0.000000000000 
+ dynamische blocksize

---
vim: spell spelllang=en
layout: post
class: post-template
cover: assets/images/monero-haproxy-icons-merged.jpg
title: 'HAProxy configuration to load-balance Monero nodes'
tags: [Articles]
author: fullmetalScience
---

To get an understanding of HAProxy's possibilities, I grabbed a list of nodes from "monero.fail":https://monero.fail and created a configuration that load-balances connections among them.

The configuration would allow you to connect Monero to HAProxy, which in turn takes care of selecting a remote node that responds fairly quickly.

However, using HAProxy makes more sense in a scenario where you want to load balance multiple public nodes that you operate yourself.

Note that there's also a project named "Monerod-Proxy":https://github.com/unyieldinggrace/monerod-proxy that offers similar functionality.


{% highlight sh %}
# HAProxy configuration file (stand-alone)
# to load-balance among a list of public Monero nodes
# /etc/haproxy/haproxy.cfg

global
  chroot /var/lib/haproxy
  user haproxy
  group haproxy
  # Enable to access information via socket file 
  # See https://www.haproxy.com/blog/haproxy-apis
  #stats socket /var/run/haproxy.sock mode 0600 level admin expose-fd listeners thread 1

defaults
  mode tcp
  # https://www.haproxy.com/blog/the-four-essential-sections-of-an-haproxy-configuration#timeout-connect-timeout-client-timeout-server
  timeout client  60s
  timeout connect  5s
  timeout server  60s

## Enable a status page at the given port
#frontend stats
#  mode http
#  bind *:8404
#  stats enable
#  stats uri /stats
#  #stats refresh 10s
#  stats admin if LOCALHOST

# Have HAProxy listen for clients on the defined port
frontend frontend
  bind :18081
  default_backend nodes

# Have HAProxy forward connections to this list of nodes
backend nodes
  # Approach the list of nodes least connected to in a roundrobin manner
  balance leastconn
  # https://www.haproxy.com/documentation/haproxy-configuration-tutorials/service-reliability/health-checks/#passive-health-checks
  # https://www.haproxy.com/blog/how-to-enable-health-checks-in-haproxy#passive-health-checks
  #
  # "Passive health checks always coexist with active health checks, with the latter
  # doing its normal polling while also being responsible for reviving a server after
  # it has been marked as down by a passive health check."
  # 
  # Active part:  Nodes will be checked every 10 minutes (inter), considered
  #               "down" if connection fails once (fall 1) and "up" if available
  #               upon the 2 further cheks (rise 2).
  # Passive part: Nodes will be considered "down" if there are at least two
  #               errors in ongoing connections (observe - error-limit 2).
  #               Only an active check (explained above) can return it to "up".
  default-server  check  inter 10m  observe layer4  error-limit 2  on-error mark-down  fall 1  rise 2
  server backend-1 node.moneroworld.com:18089
  server backend-2 uwillrunanodesoon.moneroworld.com:18089
  server backend-3 nodes.hashvault.pro:18081
  server backend-4 node1.xmr-tw.org:18081
  server backend-8 xmr-de-1.boldsuck.org:18081
  # ...
{% endhighlight %}


You may append as many nodes as you want, but be aware that each one implies more health checks and slows down HAProxy-startup.

Once HAProxy is running (@haproxy -W -f <path-to-config>@) you can test the connection with Monero RPC or CLI (@monero-wallet-cli --daemon-host <haproxy-ip> --daemon-port 18081 <further-options>@).

The configuration surely has room for improvement. If you know any tweaks, please comment in our "thread on Monero Town":https://monero.town/post/3357197.

---
vim: spell spelllang=en
layout: post
class: post-template
title: "CORONA-Report - Verifiable Research for Educational Use"
author: fullmetalScience
cover: assets/images/in-shadow-heroes-0002.jpg
tags: [Articles]
---

Corona-Report is an extract from insights gathered about the current global situation.

h2. Important update

This document was last updated in April 2020. A month later, three months into the scare, neither public health records nor scientific studies showed any evidence of a pandemic.

**Furthermore, by October 2020 it was understood that not even the existence of a virus had been proven.**


h3. Background

Technically, what is being done instead, is using data templates (software) and applying selection bias to a biological sample discarding undesired fragments until the remains are similar, but not equal, to a _pre-defined_ virus model. This newly created data becomes the model of a "new" virus, which is as fictional as the previous one.

In case of tests, real DNA sequences (found or created) are matched against negligibly tiny _fractions_ of said models.

_Effectively, all any interested party needs to know is that the illusion of viruses as invisible enemies has become a tool for misguiding people. It's really other things that make people._


h3. Resources

* "The End of Germ Theory":https://odysee.com/@spacebusters:c9/Final-The-End-of-Germ-Theory:8 is a broad primer for understanding the issue at hand.
* You may look for people listed on "The End of COVID":http://endofcovid.com (not affiliated). Many of them privide deep insights on their respective websites that may help you to master the topic.

If you have any doubts, feel free to post your question in the "Health section of Monero Town":https://monero.town/c/health. User "_fullmetalScience_":https://monero.town/u/fullmetalScience will likely notice and respond with minimum delay possible.


h2. Original document

For reference, here are the original documents. The misleading information referencing the effectively _disproven concept of pathogenic "viruses"_ will be removed when time permits:


"**Corona-Report [English]**":/assets/pdf/corona-report-0.10.0.en.pdf

"**Corona-Report [German / Deutsch]**":/assets/pdf/corona-report-0.10.0.de.pdf

"**Corona-Report [Spanish / Español]**":/assets/pdf/corona-report-0.10.0.es.pdf

Click any link above to read or download the current report in your preferred language.

_Revision-History:_

* 0.10.0 - Moved Streeck to chapter 2 and added new chapter 3
* 0.9.6 - _[German]_ Spelling corrections, _[English & Spanish]_ First-time translation - (archived: "en":/assets/pdf/corona-report-0.9.6.en.pdf, "de":/assets/pdf/corona-report-0.9.6.de.pdf, "es":/assets/pdf/corona-report-0.9.6.es.pdf)


_Further reading (this website):_
"_fullmetal.science_":/

_Article-image courtesy of Lubomir Arsov, from his visionary journey through the fragmented unconscious of the West, "In Shadow.":https://www.inshadow.net/_


{% comment %}
`signify -V -p fullmetal.pub -m corona-report.de.pdf -x corona-report.de.pdf`

{% for post in collections.posts.pages %}
#### {{post.title}}

[{{ post.title }}]({{ post.permalink }})
{% endfor %}
{% endcomment %}

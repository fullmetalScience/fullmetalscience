---
vim: spell spelllang=en
layout: post
class: post-template
title: "Prof. Püschel about the results of the first Corona study in Heinsberg (Germany)"
vid: kQZG_V_T0NY
author: kpuschel
#cover: https://img.youtube.com/vi/kQZG_V_T0NY/0.jpg
cover: assets/images/kQZG_V_T0NY_kpuschel.jpg
source: https://www.youtube.com/watch?v=kQZG_V_T0NY
tags: [Quotes]
quotes:

  22m18s:
    en: They think it is a deadly danger if you have this infection, because the numbers of the dead are presented again and again.
    es: Creen que es un peligro mortal si uno tiene esta infección, porque el número de muertos se presenta una y otra vez.
    de: Die denken das sei eine tödliche Gefahr wenn man diese Infektion hat, weil die Zahlen der Toten immer wieder präsentiert werden.

  22m31s:
    en: |
      I said it several times already: I am convinced that at the end of this year this disease will not be statistically significant in terms of the total number of deaths. [...] No more people will die this year in Germany than in the years before.
    es: |
      Ya lo he dicho varias veces: estoy convencido de que a finales de este año esta enfermedad será estadísticamente irrelevante en cuanto al número total de muertes. [...] No morirá más gente este año en Alemania que en los años anteriores.
    de: |
      Ich hab's ja verschiedene Male schon gesagt: Ich bin davon überzeugt, dass am Ende dieses Jahres diese Krankheit statistisch im Hinblick auf die Gesamtzahl der Toten überhaupt keine Rolle spielt. [...] Es sterben in diesem Jahr in Deutschland nicht mehr Menschen als in den Jahren zuvor.

  26m7s:
    en: |
      There's a very clear answer: we haven't found one yet. [...] Up to now, we have had so-called "corona deaths" in Hamburg only at the age of over 50 years - I already told you the average age - that ranges between 75 and 80.
    es: |
      Hay una respuesta muy clara: no hemos encontrado ninguno todavía. [...] Hasta ahora, hemos tenido las llamadas "muertes de la corona" en Hamburgo sólo a la edad de más de 50 años - ya le dije la edad media - aquella fluctúa entre 75 y 80.
    de: |
      Da gibt's eine ganz klare Antwort: Wir haben bisher keinen gefunden. [...] Wir haben bisher in Hamburg sogenannte "Corona-Tote" nur im Alter über 50 Jahren - ich habe Ihnen das Durchschnittsalter schon gesagt - das bewegt sich zwischen 75 und 80.

  26m24s:
    en: And all these people had serious pre-existing conditions - even those who were in their 50s. They just didn't know it, but this is also something that, at least for a forensic doctor [...], is not special.
    es: Y todas esas personas tenían serias condiciones preexistentes, incluso las que estaban en sus 50 años. No lo sabían, pero esto también es algo que, al menos para un médico forense [...] no es nada excepcional.
    de: Und alle diese Menschen hatten ernsthafte Vorerkrankungen - auch diejenigen die in den 50ern waren. Die wussten es bloß nicht, aber das ist auch etwas was zumindest für einen Rechtsmediziner [...] nichts Besonderes ist.

  27m9s:
    en: Any and all of them had fallen seriously ill before.
    es: Todos, todos se habían enfermado severamente antes.
    de: Alle, alle waren ernsthaft vorher erkrankt.
---

Medical examiner Prof. Klaus Püschel states that they have not found a single death caused by Corona and predicts zero excess mortality for this year.

English translation followed by Spanish translation, followed by transcribed German original;

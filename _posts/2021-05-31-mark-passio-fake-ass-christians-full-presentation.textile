---
vim: spell spelllang=en
layout: post
class: post-template
title: "Mark Passio - Fake Ass Christians - Full Presentation"
vid: https://vimeo.com/295807843
author: mpassio
cover: assets/images/295807843.jpg
source: https://vimeo.com/295807843
tags: [Quotes]
quotes:

  6m5s:
    en: Thinking can't be done with the emotions. Emotions are there to help us feel the repercussions of our actions toward ourselves and others, not to come to the veracity of any given subject. You can't determine truthfulness based on how something makes you feel.
    de: Mit Emotionen kann man nicht Denken. Emotionen sind dazu da, uns zu helfen, die Auswirkungen unserer Handlungen auf uns selbst und andere zu empfinden, und nicht, um die Wahrhaftigkeit eines bestimmten Themas zu bestimmen. Man kann den Wahrheitsgehalt nicht danach beurteilen, wie einen etwas fühlen lässt.

  16m10s:
    en: |
      "Religion vs Atheism" [...] is a mind-control dialectic and it's a divide-and-conquer-strategy dialectic. In two <strong>false</strong> extremes you'll always be wrong.
    de: |
      "Religion gegen Atheismus" ist eine Dialektik der Gedankenkontrolle und eine Dialektik der Teile-und-Herrsche Strategie. Innerhalb zweier <strong>falscher</strong> Extreme liegt man immer falsch.

  17m10s:
    en: I think belief is the death of freedom. I think everybody should <strong>know</strong>, not believe anything.
    de: Ich denke, Glaube ist der Tod der Freiheit. Ich denke, jeder sollte <strong>wissen</strong>, nicht etwas glauben.

  25m54s:
    en: They knew people are always trying to externalize power and seek power outside of themselves to save them from their own wrongdoings [...]. So no better thing to do than invent a new savior-myth, a new savior-religion.
    de: Sie wussten, dass Menschen immer versuchen, Macht zu externalisieren und eine Macht außerhalb von sich selbst zu suchen, die sie vor ihren eigenen Verfehlungen rettet [...]. Es war also naheliegend, einen neuen Retter-Mythos, eine neue Retter-Religion zu erschaffen.

  32m45s:
    en: The word "religion" itself comes from the Latin verb "religare". The verb religare means "to bind; to hold back by tying; or to thwart from forward progress."
    de: Das Wort "Religion" selbst kommt von dem lateinischen Verb "religare". Das Verb religare bedeutet "binden; durch Fesseln zurückhalten; oder vom Vorwärtskommen abhalten."

  37m34s:
    en: Natural Law is a set of universal, inherent, objective, non-man-made, eternal and immutable conditions which govern the consequences of behaviors of beings with the capacity or understanding the difference between harmful and non-harmful behavior.
    de: Das Naturrecht ist eine Reihe von universellen, inhärenten, objektiven, nicht von Menschen gemachten, ewigen und unveränderlichen Bedingungen, die die Folgen von Verhaltensweisen von Wesen mit der Fähigkeit oder dem Verständnis des Unterschieds zwischen schädigendem und nicht schädigendem Verhalten beherrschen.

  39m17s:
    en: It governs the consequences of behavior. [...] It isn't a behavior itself. It brings us the consequences of the behaviors that we choose. And it applies to beings who have the capacity to understand the difference between right behavior and wrong behavior.
    de: Es regelt die Folgen des Verhaltens. [...] Es ist selbst kein Verhalten. Es bringt uns die Konsequenzen der Verhaltensweisen, die wir wählen. Und es gilt für Wesen, die die Fähigkeit haben, den Unterschied zwischen richtigem Verhalten und falschem Verhalten zu kennen.

  46m35s:
    en: What you're reading about in the Bible is a being who is teaching people how to live an anarchist lifestyle - how to live with <strong>only</strong> Natural Law as a guide.
    de: Wovon wir in der Bibel lesen, ist von einem Wesen, das die Menschen lehrt, wie man einen anarchistischen Lebensstil führt - wie man mit dem Naturgesetz als <strong>alleinigem</strong> Wegweiser lebt.

  1h0m58s:
    en: True Christians reject all forms of human and worldly "authority" because they know definitively that no worldly powers can ever be higher than the God of Creation. There is no higher power than the creator of the universe and the laws of the universe.
    de: Wahre Christen lehnen alle Formen von menschlicher und weltlicher "Autorität" ab, weil sie zweifelsfrei wissen, dass keine weltliche Macht jemals höher sein kann als der Gott der Schöpfung. Es gibt keine höhere Macht als den Schöpfer des Universums und der Gesetze des Universums.

  1h35m13s:
    en: The people of this world who dwell in ignorance, apathy, laziness and cowardice are savages or beasts from a true spiritual perspective and their consciousness essentially exists only in the animal/reactive mind.
    de: Die Menschen dieser Welt, die in Unwissenheit, Apathie, Faulheit und Feigheit verweilen, sind aus wahrer spiritueller Perspektive Wilde oder Bestien und ihr Bewusstsein existiert im Wesentlichen nur im tierisch-reagierendem Verstand.

  1h35m27s:
    en: They are the true Anti-Christ, the Beast, and their thoughts and behaviors work continually to kill the true Christ Consciousness.
    de: Sie sind der wahre Anti-Christ, die Bestie, und ihre Gedanken und Verhaltensweisen arbeiten unaufhörlich daran, das wahre Christus-Bewusstsein zu töten.

  1h41m58s:
    en: Everybody wants to take their prejudice into the truth. That's the problem.
    de: Jeder will seine vorgefaßte Ansicht in die Wahrheit mitnehmen. Das ist das Problem.

  2h19m54s:
    en: I've been teaching this for ten years. Michael Tsarion's been teaching it for fifteen to twenty years. Jordan Maxwell's been teaching it for God-knows how long.
    de: Ich vermittle das schon seit zehn Jahren. Michael Tsarion lehrt es schon seit fünfzehn bis zwanzig Jahren. Jordan Maxwell lehrt es schon seit weiß-Gott wie lange.

  2h24m52s:
    en: They know that these laws exist to constrict behavior and bring chaotic consequence for bad behavior.
    de: Sie wissen, dass diese Gesetze existieren, um Verhalten zu regulieren und negative Konsequenzen für schadhaftes Verhalten mit sich zu bringen.

  2h25m51s:
    en: They believe [...] that they're in a prison, because they know that the universe is governed by Moral Law and they themselves cannot do whatever they want without consequence.
    de: Sie glauben [...], dass sie sich in einem Gefängnis befinden, weil sie wissen, dass das Universum von einem moralischen Gesetz regiert wird und sie selbst nicht tun können, was sie wollen, ohne dass es Konsequenzen hätte.

  2h28m33s:
    en: |
      They call human beings "the dead" [...] and their rationale went like this: If you're not using you intelligence, the thought aspect of consciousness is deadened. If you're not using your care, then the emotional aspect of consciousness is deadened.
    de: |
      Sie nennen die Menschen "die Toten" [...] und ihre Begründung ging so: Wenn du deine Intelligenz nicht einsetzt, dann ist der gedankliche Aspekt des Bewusstseins abgestorben. Wenn du dein Mitgefühl nicht einsetzt, dann ist der emotionale Aspekt des Bewusstseins abgestorben.

  2h29m1s:
    en: And if you're lazy, apathetic and cowardly, and you don't take [...] real world action, then the "actions" component of consciousness is dead. And therefore, if someone is dead in thought, emotion and action, their entire consciousness is dead.
    de: Und wenn du faul, apathisch und feige bist und in der Welt keine bedeutungsvollen Handlungen setzt, dann ist die Aktions-Komponente  des Bewusstseins tot. Folglich, wenn jemand tot ist im Denken, im Fühlen und im Handeln, dann ist sein ganzes Bewusstsein tot.

  2h29m16s:
    en: And therefore the being can be said not to be truly alive. They're just a flesh-robot that they could do whatever they want to, like property. That is their rationale for how they will treat other human beings.
    de: Und deshalb kann man sagen, dass das Wesen nicht wirklich lebendig ist. Es ist nur ein Fleisch-Roboter, mit dem sie machen können, was auch immer sie möchten, wie mit Eigentum. Das ist ihre Grundlage andere um Menschen entsprechend zu behandeln.

  2h31m36s:
    en: No Order-Follower is a True Christian, because True Christians follow Conscience, not orders.
    de: Kein Befehlsempfänger ist ein wahrer Christ, denn wahre Christen folgen dem Gewissen, nicht Befehlen.

  3h14m35s:
    en: The law of freedom states, that as morality increases in the aggregate of any given population of souls, that society's collective freedom will also increase. Conversely as any society's aggregate morality declines, that society's freedom will proportionally decline.
    de: Das Gesetz der Freiheit bestimmt, dass mit zunehmender Moral in der Gesamtheit einer gegebenen Population auch die kollektive Freiheit dieser Gesellschaft zunimmt. Nimmt umgekehrt die Gesamtmoral innerhalb einer Gesellschaft ab, wird die Freiheit dieser Gesellschaft proportional abnehmen.

  3h14m53s:
    en: |
      In other words: As morality increases, freedom increases.
    de: |
      Mit anderen Worten: Zunehmende Moral erweitert Freiheit.

  3h52m38s:
    en: Intention is meaningless. Action is everything. Intention is meaningless. Behavior is everything.
    de: Absichten sind bedeutungslos. Handlungen sind alles. Absichten sind bedeutungslos. Verhalten ist alles.

  3h58m46s:
    en: Immoral behavior, by definition is coercive and violent behavior which causes harm to another being's life, rights or property.
    de: Unmoralisches Verhalten ist per Definition gewaltsames Verhalten, das dem Leben, den Rechten oder dem Eigentum eines anderen Lebewesens schadet.

  3h58m55s:
    en: Any action that does not infringe on another's rights or cause harm to another sentient being is a right. <strong>Actual</strong> wrongdoing is a violation of rights.
    de: Jede Handlung, die nicht gegen die Rechte eines anderen verstößt oder einem anderen fühlenden Wesen schadet, ist ein Recht. <strong>Wirkliches</strong> Fehlverhalten ist eine Verletzung von Rechten.

  4h27m52s:
    en: Most people would make that deal. They <strong>want</strong> to be the controllers. They want to be the ruling class. [...] They love the concept, because they want it to be them.
    de: Die meisten Leute würden diesen Pakt schließen. Sie <strong>wollen</strong> die Regierenden sein. Sie wollen die herrschende Klasse sein. [...] Sie lieben das Konzept, weil sie es selbst sein wollen.

  5h1m10s:
    en: The New Testament adage to "turn the other cheek" meant to continue to try to bring the Truth to people you care about over and over, even if you are rejected, ridiculed, called crazy, etc. It meant to endure that type of rejection of your message of Truth.
    de: Das neutestamentliche Sprichwort "die andere Wange hinhalten" bedeutete, fortwährend zu versuchen, den Menschen, die einem wichtig sind, immer wieder die Wahrheit zu vermitteln, auch wenn man abgelehnt, verspottet, als verrückt bezeichnet wird, usw. Es bedeutete, diese Art der Ablehnung ihrer Botschaft der Wahrheit zu ertragen.

  5h2m0s:
    en: True Forgiveness does <strong>not</strong> mean continuing to excuse the willful commission of wrongdoing an infinitive number of times. That is naiveté at best and cooperation with Evil at worst.
    de: Wahre Vergebung bedeutet <strong>nicht</strong>, das vorsätzliche Begehen von Fehlverhalten unendlich oft zu entschuldigen. Das ist im besten Fall Naivität und im schlimmsten Fall Kooperation mit dem Bösen.

  5h4m35s:
    en: Pacifism is nothing more than a euphemism for victim mentality and it has <strong>nothing</strong> to do with True Christianity.
    de: Pazifismus ist nichts weiter als ein Euphemismus für Opfermentalität und hat <strong>nichts</strong> mit wahrem Christentum zu tun.
---


A couple of minutes worth reading cannot possibly replace several hours of information, but they might confuse you just enough to start asking the right questions.

English original followed by German translation;
